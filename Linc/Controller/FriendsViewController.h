//
//  FriendsViewController.h
//  Linc
//
//  Created by Daniel.Burke on 11/15/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserCell.h"

@interface FriendsViewController : UIViewController
<
UITableViewDataSource,
UITableViewDelegate
>

@property (strong, nonatomic) NSMutableArray *friends;
@property (strong, nonatomic) NSMutableArray *contacts;
@property (strong, nonatomic) NSMutableArray *filtered;
@property (strong, nonatomic) NSMutableArray *selectedContacts;
@property (strong, nonatomic) NSMutableArray *selectedIndexPaths;

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UITableView *contactsTableView;
@end
