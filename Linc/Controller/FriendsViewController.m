//
//  FriendsViewController.m
//  Linc
//
//  Created by Daniel.Burke on 11/15/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "FriendsViewController.h"
#import <AddressBookUI/AddressBookUI.h>

@interface FriendsViewController ()

@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    _friends = [[NSMutableArray alloc] init];
    _filtered = [[NSMutableArray alloc] init];
    _contacts = [[NSMutableArray alloc] init];
    _selectedContacts = [[NSMutableArray alloc] init];
    _selectedIndexPaths = [[NSMutableArray alloc] init];
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        NSLog(@"Denied");
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        [self getContacts];
    } else{
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (!granted){
                NSLog(@"Just denied");
                return;
            }
            [self getContacts];
        });
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [_searchTextField addTarget:self action:@selector(searchContacts:) forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchContacts:(UITextField*)textField{
    if([textField.text length]){
        _filtered = [[NSMutableArray alloc] init];
        for(NSDictionary *contact in _contacts){
            NSString *name = [NSString stringWithFormat:@"%@ %@", [contact objectForKey:@"firstName"],[contact objectForKey:@"lastName"]];
            if([[name lowercaseString] containsString:[textField.text lowercaseString]]){
                [_filtered addObject:contact];
            }
        }
    }
    else{
        _filtered = _contacts;
    }
    [_contactsTableView reloadData];
}

- (void)getContacts{
    CFErrorRef *error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL,error);
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
    
    for(int i = 0; i < numberOfPeople; i++) {
        
        NSMutableDictionary *contact = [[NSMutableDictionary alloc] init];
        
        ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
        
        NSString *firstName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
        
        if(firstName){
            NSLog(@"%@ length: %i", firstName, (int)[firstName length]);
            [contact setObject:firstName forKey:@"firstName"];
        }
        
        if(lastName){
            NSLog(@"%@ length: %i", lastName, (int)[firstName length]);
            [contact setObject:lastName forKey:@"lastName"];
        }
        
        if(firstName || lastName){
            ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++) {
                NSString *phoneNumber = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phoneNumbers, i);
                
                if(phoneNumber){
                    [contact setObject:phoneNumber forKey:@"phoneNumber"];
                }
            }
            
            [_contacts addObject:contact];
        }
    }
    _filtered = [_contacts mutableCopy];
    [_contactsTableView reloadData];

}

#pragma mark - Keyboard Methods
-(void)keyboardWillShow:(NSNotification*)notification{
    UIEdgeInsets notesInset = _contactsTableView.contentInset;
    
    // keyboard frame is in window coordinates
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    notesInset = UIEdgeInsetsMake(0, 0, 40 + keyboardFrame.size.height-64, 0);
    [UIView animateWithDuration:0.34 animations:^{
        _contactsTableView.contentInset = notesInset;
    }];
}

-(void)keyboardWillHide:(NSNotification*)notification{
    [UIView animateWithDuration:0.34 animations:^{
        _contactsTableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserCell *cell = (UserCell*)[tableView cellForRowAtIndexPath:indexPath];
    if([_selectedIndexPaths containsObject:indexPath]){
        [_selectedIndexPaths removeObject:indexPath];
        [_selectedContacts removeObject:[_selectedContacts objectAtIndex:indexPath.row]];
        [_filtered insertObject:[_selectedContacts objectAtIndex:indexPath.row] atIndex:0];
        cell.selected = NO;
    }
    else{
        cell.selected = YES;
        [_selectedIndexPaths addObject:indexPath];
        [_selectedContacts addObject:[_filtered objectAtIndex:indexPath.row]];
        [_filtered removeObject:[_filtered objectAtIndex:indexPath.row]];
    }
    [_contactsTableView reloadData];
    NSLog(@"Share to friends: %@", _selectedContacts);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //Later, calc height based on text in comment
    return  44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([_selectedContacts count]){
        return 2;
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([_selectedContacts count]){
        switch (section) {
            case 0:
                return [_selectedContacts count];
                break;
            case 1:
                return [_filtered count];
            default:
                break;
        }
    }
    return [_filtered count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Friend";
    UserCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[UserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if([_selectedContacts count]){
        switch (indexPath.section) {
            case 0:{
                NSDictionary *contact = [_selectedContacts objectAtIndex:indexPath.row];
                cell.selected = YES;
                cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", [contact objectForKey:@"firstName"], [contact objectForKey:@"lastName"]];
                break;
            }
            case 1:{
                NSDictionary *contact = [_filtered objectAtIndex:indexPath.row];
                cell.selected = YES;
                cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", [contact objectForKey:@"firstName"], [contact objectForKey:@"lastName"]];
                break;
            }
            default:
                break;
        }
    }
    else{
        NSDictionary *contact = [_filtered objectAtIndex:indexPath.row];
        cell.selected = ([_selectedIndexPaths containsObject:indexPath]) ? YES : NO;
        cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", [contact objectForKey:@"firstName"], [contact objectForKey:@"lastName"]];
    }
    return cell;
}

@end
