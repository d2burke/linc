//
//  LincListViewController.h
//  Linc
//
//  Created by Daniel.Burke on 11/11/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LincListViewController : UIViewController

@property (strong, nonatomic) PFUser *currentUser;

@end
