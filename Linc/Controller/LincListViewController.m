//
//  LincListViewController.m
//  Linc
//
//  Created by Daniel.Burke on 11/11/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "LincListViewController.h"

@interface LincListViewController ()

@end

@implementation LincListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"userObjectId"]){
        NSString *objectId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userObjectId"];
        PFQuery *query = [PFUser query];
        [query whereKey:@"objectId" equalTo:objectId]; // find all the women
        NSArray *users = [query findObjects];
        _currentUser = [users objectAtIndex:0];
        NSLog(@"Current User: %@", _currentUser);
    }
    else{
        [self performSegueWithIdentifier:@"ShowLogin" sender:self];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //Later, calc height based on text in comment
    return  100;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"LinkCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = @"Table Cell";
    
    return cell;
}



#pragma mark - Global Clipboard Copy
- (void)userDidCopyString:(NSNotification*)notification{
    UIPasteboard *pasteboard;
    pasteboard = [UIPasteboard generalPasteboard];
    if([pasteboard containsPasteboardTypes:UIPasteboardTypeListURL]){
        NSLog(@"%@ is a URL", pasteboard.string);
    }
    else{
        NSLog(@"%@ is NOT a URL", pasteboard.string);
    }
}

@end
