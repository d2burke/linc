//
//  ReadViewController.h
//  Linc
//
//  Created by Daniel.Burke on 11/11/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadViewController : UIViewController
<
UINavigationControllerDelegate
>

@property (weak, nonatomic) IBOutlet UILabel *articleTitleLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *articleImageView;
@property (weak, nonatomic) IBOutlet UITableView *articleTableView;

- (IBAction)goBack:(id)sender;

@end
