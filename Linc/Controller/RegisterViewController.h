//
//  RegisterViewController.h
//  Linc
//
//  Created by Daniel.Burke on 11/12/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Config.h"

@interface RegisterViewController : UIViewController
<
UITextFieldDelegate,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate
>

@property (nonatomic) BOOL formIsValid;
@property (strong, nonatomic) NSData *userImageData;
@property (strong, nonatomic) PFUser *currentUser;

@property (weak, nonatomic) IBOutlet UIButton *addUserImageButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UITextField *previousTextField;
@property (strong, nonatomic) IBOutlet UITextField *currentTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmTextField;

- (IBAction)registerWithEmail:(id)sender;


@end
