//
//  RegisterViewController.m
//  Linc
//
//  Created by Daniel.Burke on 11/12/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _formIsValid = NO;
    _previousTextField = _nameTextField;
    _registerButton.layer.cornerRadius = 6.f;
    _addUserImageButton.layer.cornerRadius = 50.f;
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
    _currentUser = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if(_currentUser){
        NSLog(@"Current User: %@", _currentUser);
    }
    else{
        NSLog(@"No User");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*!
 *  Description     Save user image to UserDefaults and to remote storage
 */
- (void)saveUserImage:(UIImage*)image{
    [_addUserImageButton setImage:image forState:UIControlStateNormal];
}

#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    _currentTextField = (UITextField*)textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    _previousTextField = (UITextField*)textField;
    if(textField == _emailTextField){
        NSArray *emailComponents = [_emailTextField.text componentsSeparatedByString:@"@"];
        if([emailComponents count] > 1){
            _usernameTextField.text = [emailComponents objectAtIndex:0];
        }
        [_usernameTextField becomeFirstResponder];
    }
    _formIsValid = [self formFieldsAreValid];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == _nameTextField){
        [_emailTextField becomeFirstResponder];
    }
    else if(textField == _emailTextField){
        NSArray *emailComponents = [_nameTextField.text componentsSeparatedByString:@"@"];
        if([emailComponents count] > 1){
            _usernameTextField.text = [emailComponents objectAtIndex:0];
        }
        [_usernameTextField becomeFirstResponder];
    }
    else if(textField == _usernameTextField){
        [_passwordTextField becomeFirstResponder];
    }
    else if(textField == _passwordTextField){
        [_confirmTextField becomeFirstResponder];
    }
    else if(textField == _confirmTextField){
        [textField resignFirstResponder];
    }
    
    _formIsValid = [self formFieldsAreValid];
    
    return YES;
}

/*!
 *  Description     Validate register form fields for proper values. Only
 *                  validate fields that follow the current field, unless
 *                  the user has gone through each field at least once.
 *                  Track this by setting the field's tag to 1 when it has
 *                  been visited.
 *
 *  return          Whether or not all fields contain valid input
 */
- (BOOL)formFieldsAreValid{
    
    //Set this field to visited (tag = 1)
    _previousTextField.tag = 1;
    
    //If the latest edited field is empty, return NO
    UIColor *placeHolderColor = (![_previousTextField.text length]) ? [UIColor redColor] : [UIColor whiteColor];
    _previousTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_previousTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: placeHolderColor}];

    if(![_previousTextField.text length]){
        return NO;
    }
    
    //Check all previous fields for validation as well
    NSArray *nameComponents = [_nameTextField.text componentsSeparatedByString:@" "];
    if(![_nameTextField.text length] || [nameComponents count] < 2){
        _nameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_nameTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        _nameTextField.textColor = [UIColor redColor];
        return NO;
    }
    else{
        _nameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_nameTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        _nameTextField.textColor = [UIColor blackColor];
    }
    
    if(![_usernameTextField.text length] > 4){
        _usernameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_usernameTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        _usernameTextField.textColor = [UIColor redColor];
        return NO;
    }
    else{
        _usernameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_usernameTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        _usernameTextField.textColor = [UIColor blackColor];
    }
    
    if(![_emailTextField.text length]){
        _emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_emailTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        _emailTextField.textColor = [UIColor redColor];
        return NO;
    }
    else{
        _emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_emailTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        _emailTextField.textColor = [UIColor blackColor];
    }
    
    if(![_passwordTextField.text length] && [_passwordTextField.text isEqualToString:_confirmTextField.text]){
        _passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_passwordTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        _passwordTextField.textColor = [UIColor redColor];
        return NO;
    }
    else{
        _passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_passwordTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        _passwordTextField.textColor = [UIColor blackColor];
    }
    
    if(![_confirmTextField.text length] && [_passwordTextField.text isEqualToString:_confirmTextField.text]){
        _confirmTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_confirmTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        _confirmTextField.textColor = [UIColor redColor];
        return NO;
    }
    else{
        _confirmTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_confirmTextField.attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        _confirmTextField.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (IBAction)registerWithEmail:(id)sender {
    [self formFieldsAreValid];
    if(_formIsValid){
        PFUser *user = [PFUser user];
        user.username = _usernameTextField.text;
        user.password = _passwordTextField.text;
        user.email = _emailTextField.text;
        
        NSArray *nameComponents = [_nameTextField.text componentsSeparatedByString:@" "];
        user[@"FirstName"] = [nameComponents objectAtIndex:0];
        user[@"LastName"] = ([nameComponents count] > 1) ? [nameComponents objectAtIndex:1] : @"";
        
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [[NSUserDefaults standardUserDefaults] setObject:[user objectId] forKey:@"userObjectId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self dismissViewControllerAnimated:YES completion:^{
                    //
                }];
            } else {
                NSString *errorString = [error userInfo][@"error"];
                NSLog(@"Parse: %@", errorString);
            }
        }];
    }
    else{
        NSLog(@"Form is Not Valid");
    }
}

@end
