//
//  Config.h
//  Sift
//
//  Created by Daniel.Burke on 7/7/14.
//  Copyright (c) 2014 Forrent.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define ICON_FONT [UIFont fontWithName:@"icomoon" size:24.f]
#define LARGE_ICON_FONT [UIFont fontWithName:@"icomoon" size:36.f]
#define SEARCH_BASE_URL @"http://heliosapi.homes.com/v1/listings/search?status=for+rent&api_key=0000000000000000000000000000000&pagesize=50&sort=property_type+asc&supplier_id=223"

#define GUESTCARD_URL @"http://api.forrent.com/guestcard/external/submit"
#define TEST_GUESTCARD_URL @"http://api.helium.forrent.com/guestcard/external/submit"

@interface Config : NSObject

@end
