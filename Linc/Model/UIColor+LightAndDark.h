//
//  UIColor+LightAndDark.h
//  FM
//
//  Created by Daniel.Burke on 7/22/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

@import UIKit;

@interface UIColor (LightAndDark)

- (UIColor *)lighterColor;
- (UIColor *)darkerColor;

@end
