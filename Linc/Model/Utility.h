//
//  Utility.h
//  Occupied
//
//  Created by Daniel Burke on 1/21/14.
//  Copyright (c) 2014 Grow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"

@protocol UtilityDelegate <NSObject>

@optional
- (void)didChoosePhoto:(NSDictionary*)photo;

@end

typedef enum {
    MediaOptionCamera,
    MediaOptionPhotos
} MediaOption;

typedef enum {
    EmailSocialNetwork,
    FacebookSocialNetwork,
    TwitterSocialNetwork,
    GooglePlusSocialNetwork,
    PinterestSocialNetwork
} SocialNetworkType;

typedef enum {
    EmptyDataSetAfterSiftingAll,
    EmptyDataSetAfterNoResultsSearch
} EmptyDataSetReasonType;

typedef enum {
    ViewTypeCards,
    ViewTypeList
} ViewType;

typedef enum{
    SwipeDirectionLeft,
    SwipeDirectionRight,
    SwipeDirectionCenter
} SwipeDirection;

typedef enum{
    SaveListingActionType,
    SaveLocationActionType
} ActionType;

typedef enum {
    ListingAnnotationType,
    SavedAnnotationType,
    CustomAnnotationType,
    HiddenAnnotationType,
    SelectedAnnotationType,
    ApartmentAnnotationType,
    ResidentialAnnotationType,
    HighSchoolAnnotationType,
    MiddleSchoolAnnotationType,
    PrimarySchoolAnnotationType
} AnnotationType;

typedef enum {
    AutoCompleteEntityTypeSchool,
    AutoCompleteEntityTypeCity,
    AutoCompleteEntityTypeNeighborhood,
    AutoCompleteEntityTypeSite
} AutoCompleteEntityType;

typedef enum {
    SortKeyPriceUp,
    SortKeyPriceDown,
    SortKeyBedsUp,
    SortKeyBedsDown,
    SortKeyBathsUp,
    SortKeyBathsDown,
    SortKeyDistanceUp,
    SortKeyDistanceDown
} SortKey;

@interface Utility : NSObject <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) id <UtilityDelegate> delegate;
//API CALL
- (void)   apiCall:(NSString *)endPoint
    withParameters:(NSDictionary *)params
        completion:(void (^)(id responseObject))completion
           failure:(void (^)(NSError *error))failed;

//API CALL WITH OPTION TO SPECIFY THE HTTP METHOD
- (void)   apiCall:(NSString *)endPoint
    withParameters:(NSDictionary *)params
         andMethod:(NSString *)method
        completion:(void (^)(id responseObject))completion
           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failed;

//Handle XML Response Type
- (void)apiCallWithXML:(NSString *)endPoint
        withParameters:(NSDictionary *)params
            completion:(void (^)(id responseObject))completion
               failure:(void (^)(NSError *error))failed;

//Cancel all AFNetworing calls
- (void)cancelAllCalls;

//RETURNS A READABLE STRING WITH THE CURRENT TIME
- (NSString *)nowString;

//RETURNS A READABLE STRING FROM AN NSDATE OBJECT
- (NSString *)readableDate:(NSDate *)date withType:(NSString *)type;

//
- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval;

//GET AN IMAGE OF A SOLID COLOR
- (UIImage *)imageWithColor:(UIColor *)color andRect:(CGRect)rect;

//Choose a photo from your photo roll
- (IBAction)choosePhoto:(id)sender;

//Take a photo with your camera
- (IBAction)takePhoto:(id)sender;

//Create an acceptable price range string for listings
- (NSString*)normalizedPriceString:(NSDictionary*)price;
@end
