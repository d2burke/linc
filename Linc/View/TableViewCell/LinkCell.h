//
//  LinkCell.h
//  Linc
//
//  Created by Daniel.Burke on 11/11/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *lincContainer;
@property (weak, nonatomic) IBOutlet UIButton *userImageButton;

@end
