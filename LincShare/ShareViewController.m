//
//  ShareViewController.m
//  LincShare
//
//  Created by Daniel.Burke on 11/12/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "ShareViewController.h"
#import "UserCell.h"

@interface ShareViewController ()

@end

@implementation ShareViewController

- (void)viewDidLoad{
    self.view.alpha = 0;
//    NSUserDefaults *userDefaults = [[NSUserDefaults alloc]initWithSuiteName:@"group.Linc"];
    _friends = [@[@"Ronnie",@"Bobby",@"Ricky",@"Mike"] mutableCopy];
    _containerView.layer.cornerRadius = 6.f;
    _selectedIndexPaths = [[NSMutableArray alloc] init];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.alpha = 1;
    }];
}

- (void)viewDidAppear:(BOOL)animated{
    //pull the URL out
    NSExtensionItem *item = self.extensionContext.inputItems[0];
    NSItemProvider *provider = item.attachments[0];
    if ([provider hasItemConformingToTypeIdentifier:@"public.url"]) {
        [provider loadItemForTypeIdentifier:@"public.url" options:nil completionHandler:^(id<NSSecureCoding> item, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = (NSURL*)item;
                _linkLabel.text = url.absoluteString;
                NSLog(@"Link: %@", url.absoluteString);
            });
        }];
    }
    else{
        NSLog(@"No Link");
    }
}

#pragma mark - UITableView Delegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserCell *cell = (UserCell*)[tableView cellForRowAtIndexPath:indexPath];
    if([_selectedIndexPaths containsObject:indexPath]){
        [_selectedIndexPaths removeObject:indexPath];
        cell.selected = NO;
    }
    else{
        cell.selected = YES;
        [_selectedIndexPaths addObject:indexPath];
    }
    NSLog(@"Share to %i friends", (int)[_selectedIndexPaths count]);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //Later, calc height based on text in comment
    return  44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_friends count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"UserCell";
    UserCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[UserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selected = ([_selectedIndexPaths containsObject:indexPath]) ? YES : NO;
    cell.nameLabel.text = [_friends objectAtIndex:indexPath.row];
    return cell;
}

- (IBAction)dismiss {
    [UIView animateWithDuration:0.34 animations:^{
        self.view.alpha = 0;
    } completion:^(BOOL finished) {
        [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
    }];
}

@end
