//
//  UserCell.m
//  Linc
//
//  Created by Daniel.Burke on 11/12/14.
//  Copyright (c) 2014 D2 Development. All rights reserved.
//

#import "UserCell.h"

@implementation UserCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _selectionView.backgroundColor = (selected) ? [UIColor grayColor] : [UIColor clearColor];
}

- (void)layoutSubviews{
    _selectionView.layer.cornerRadius = 10.f;
    _selectionView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _selectionView.layer.borderWidth = 2.f;
}

@end
